import 'dart:io';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/io.dart';

import 'MyHttpClient.dart';
import 'env_util.dart';

String? token;

void main() async {
  HttpOverrides.global = MyHttpOverrides();

  final env = await loadEnvFile('env/.env_dev');
  token = env['TOKEN'];

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder(
              stream: _channel.stream,
              builder: (context, snapshot) =>
                  Text(snapshot.hasData ? snapshot.data : 'No data'),
            ),
            InkWell(
              onTap: makeSomeMessage,
              child: const Text('Click me'),
            )
          ],
        ),
      ),
    );
  }

  final _channel = IOWebSocketChannel.connect(
    Uri.parse('wss://ps.dev.instadev.net:9090'),
    headers: {
      'token': 'Bearer $token'
    }
  );

  void makeSomeMessage() {
    try {
      _channel.sink.add('{\"create_message\": {\"content\": \"Текст сообщения\"},\"chat_id\": 33}');
      print('SEND!');
    } catch (ex) {
      print('CANNOT SEND because of ${ex.toString()}');
    }
  }

  @override
  void dispose() {
    _channel.sink.close();
    super.dispose();
  }
}
